package hometask4;

import java.util.*;
import java.lang.Float;

class EDublicateElementException extends Exception {
    public EDublicateElementException(String errorMessage) {
        super(errorMessage);
    }
}

public class MathBox {
    private ArrayList<Number> Colect;

    @Override
    public String toString() {
        return "MathBox{" +
                "Colect=" + Colect +
                '}';
    }

    // контсруктор
    public MathBox(Number[] p_Number) throws EDublicateElementException {
        //
        Colect = new ArrayList<Number>();
        for (int i = 0; i < p_Number.length; i++) {
            if (Colect.contains(p_Number[i])) {
                // сообщение о дублировании элементов массива
                throw new EDublicateElementException("Недопускается дублирование элементов массива");
            } else {
                Colect.add(p_Number[i]);
            }
        }
        //Colect.addAll(Arrays.asList(p_Number));
    }
    public Number sumNumbers(Number pN1, Number pN2) {
        if(pN1 instanceof Double || pN2 instanceof Double) {
            return pN1.doubleValue() + pN2.doubleValue();
        } else if(pN1 instanceof Float || pN2 instanceof Float) {
            return pN1.floatValue() + pN2.floatValue();
        } else if(pN1 instanceof Long || pN2 instanceof Long) {
            return pN1.longValue() + pN2.longValue();
        } else {
            return pN1.intValue() + pN2.intValue();
        }
    }
    public Number summator () {
        Number vRes = 0;
        for (Number Item: Colect) {
            vRes = sumNumbers(vRes , Item);
        }
        return vRes;
    }
    public void splitter(Number p_Divisor) {
        for (int i = 0; i < Colect.size(); i++) {
            Number Item = Colect.get(i);
            if (Item instanceof Double || p_Divisor instanceof Double) {
                Item = Item.doubleValue() / p_Divisor.doubleValue();
            } else {
                Item = Item.floatValue() / p_Divisor.floatValue();
            }
            Colect.set(i, Item);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MathBox)) return false;
        MathBox mathBox = (MathBox) o;
        return Colect.equals(mathBox.Colect);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Colect);
    }
}
