package homeTask4MathBox;

import java.util.Map;
import java.util.Set;

public interface IMathBox {
    // а) добавление/удаление
    public void add(Integer value);
    public void remove(Integer value);
    // б) метод getTotal(), возвращающий сумму всех объектов
    public long getTotal();
    // в) метод getAverage(), возвращающий среднее значение вхождений
    @ClearData
    @LogData
    public double getAverage();
    // г) метод getMultiplied(int multiplicator), возвращающий новую коллекцию, в которой все элементы умножены на множитель. Коллекция в объекте при этом изменяется
    @LogData
    public Set<Integer> getMultiplied(int multiplicator);
    // д) метод getMap(), возвращающий Map<Integer, String>, в которой ключи и значения это одни и те же элементы, приведенные к разным типам.
    public Map<Integer, String> getMap();
    // е) метод getMax(), возвращающий максимальное число
    public long getMax();
    // ж) метод getMin(), возвращающий минимальное число
    public long getMin();
}
