package homeTask4MathBox.visitors;

import java.util.HashMap;
import java.util.Map;

public class VisitorMap extends CustomVisitor {
    private Map<Integer, String> result = new HashMap<>();

    @Override
    public void processValue(Integer value) {
        result.put(value, value.toString());
    }

    public Map<Integer, String> getResult() {
        return result;
    }
}
