package homeTask4MathBox.visitors;

public class VisitorMax extends CustomVisitor {

    public VisitorMax() {
        resultValue = Integer.MIN_VALUE;
    }

    @Override
    public void processValue(Integer value) {
        resultValue = Long.max(resultValue, value);
    }
}
