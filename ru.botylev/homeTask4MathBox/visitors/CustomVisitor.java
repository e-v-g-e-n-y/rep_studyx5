package homeTask4MathBox.visitors;

public abstract class CustomVisitor {
    protected long resultValue;

    public CustomVisitor() {
        resultValue = 0;
    }

    public abstract void processValue(Integer value);

    public long getResultValue() {
        return resultValue;
    }
}
