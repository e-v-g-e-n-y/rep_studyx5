package homeTask4MathBox.visitors;

public class VisitorMin extends CustomVisitor {
    public VisitorMin() {
        resultValue = Integer.MAX_VALUE;
    }

    @Override
    public void processValue(Integer value) {
        resultValue = Long.min(resultValue, value);
    }
}
