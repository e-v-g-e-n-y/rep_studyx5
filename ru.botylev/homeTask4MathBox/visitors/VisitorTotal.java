package homeTask4MathBox.visitors;

public class VisitorTotal extends CustomVisitor {
    @Override
    public void processValue(Integer value) {
        resultValue = resultValue + value;
    }
}
