package homeTask4MathBox;

import java.lang.reflect.Proxy;
import java.util.Map;

public class Main4 {
    public static void main(String[] args) {
        MathBox mb = new MathBox();
        mb.add(1);
        mb.add(3);
        mb.add(7);
        mb.add(-5);
        System.out.println(mb.toString());
        System.out.println("getTotal=" + mb.getTotal());
        System.out.println("getAverage=" + mb.getAverage());
        System.out.println("getMax=" + mb.getMax());
        System.out.println("getMin=" + mb.getMin());
        mb.getMultiplied(4);
        System.out.println(mb.toString());
        Map<Integer, String> result = mb.getMap();
        System.out.println(result.toString());

        MathBoxProxy proxy = new MathBoxProxy(mb);

        IMathBox mathProxy = (IMathBox)Proxy.newProxyInstance(
                MathBoxProxy.class.getClassLoader(),
                new Class[]{IMathBox.class},
                proxy);

        // демонстрация логирования
        mathProxy.getMultiplied(2);

        // демонтрация очистка при наличии аннотации
        mathProxy.getAverage();
        System.out.println(mathProxy.toString());
    }
}
