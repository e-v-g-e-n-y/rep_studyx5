package homeTask4MathBox;

public class ERuntimeMathBoxException extends RuntimeException {

    public ERuntimeMathBoxException(String message) {
        super(message);
    }
}
