package homeTask4MathBox;

import homeTask4MathBox.visitors.*;

import java.util.*;

public class MathBox implements IMathBox {
    private final String errStringEmptySet = "Массив пуст!";
    private Set<Integer> numbers;

    public MathBox() {
        numbers = new HashSet<>();
    }
    public void cleanUp() { numbers.clear(); }
    @Override
    public String toString() {
        return "MathBox{" +
                "numbers=" + Arrays.toString(numbers.toArray()) +
                '}';
    }

    protected void processVisitor(CustomVisitor visitor){
        Iterator<Integer> setIterator = numbers.iterator();
        while(setIterator.hasNext()){
            visitor.processValue(setIterator.next());
        }
    }

    // а) добавление/удаление
    public void add(Integer value) {
        numbers.add(value);
    }
    public void remove(Integer value) { numbers.remove(value); }

    // б) метод getTotal(), возвращающий сумму всех объектов
    public long getTotal() {
        CustomVisitor visitor = new VisitorTotal();
        processVisitor(visitor);
        return visitor.getResultValue();
    }

    // в) метод getAverage(), возвращающий среднее значение вхождений
    public double getAverage(){
        if (numbers.isEmpty()) {throw new ERuntimeMathBoxException(errStringEmptySet);}
        return (double)getTotal()/numbers.size();
    }

    // г) метод getMultiplied(int multiplicator), возвращающий новую коллекцию, в которой все элементы умножены на множитель. Коллекция в объекте при этом изменяется
    public Set<Integer> getMultiplied(int multiplicator) {
        Set<Integer> temp = new HashSet<>();
        for(Integer i : numbers)
            temp.add(i*multiplicator);
        numbers.clear();
        numbers.addAll(temp);
        return temp;
    }

    // д) метод getMap(), возвращающий Map<Integer, String>, в которой ключи и значения это одни и те же элементы, приведенные к разным типам.
    public Map<Integer, String> getMap() {
        VisitorMap visitor = new VisitorMap();
        processVisitor(visitor);
        return visitor.getResult();
    }

    // е) метод getMax(), возвращающий максимальное число
    public long getMax() {
        if (numbers.isEmpty()) {throw new ERuntimeMathBoxException(errStringEmptySet);}
        CustomVisitor visitor = new VisitorMax();
        processVisitor(visitor);
        return visitor.getResultValue();
    }

    // ж) метод getMin(), возвращающий минимальное число
    public long getMin() {
        if (numbers.isEmpty()) {throw new ERuntimeMathBoxException(errStringEmptySet);}
        CustomVisitor visitor = new VisitorMin();
        processVisitor(visitor);
        return visitor.getResultValue();
    }
}
