package homeTask4MathBox;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MathBoxProxy implements InvocationHandler {
    private MathBox mathBox;

    public MathBoxProxy(MathBox mathBox) {
        this.mathBox = mathBox;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        boolean needLog     = false;
        boolean needCleanUp = false;
        Annotation[] vAnnts = method.getDeclaredAnnotations();
        for (int i = 0; i < vAnnts.length; i++) {
            //System.out.println("Anotation:" + vAnnts[i].toString());
            if (vAnnts[i].toString().equals("@homeTask4MathBox.LogData()"))   { needLog     = true; }
            if (vAnnts[i].toString().equals("@homeTask4MathBox.ClearData()")) { needCleanUp = true; }
        }
        if (needLog) { System.out.println("logging method '" + method.getName() + "' before:" +mathBox.toString());}
        Object obj =  method.invoke(mathBox, args);
        if (needCleanUp) { mathBox.cleanUp(); }
        if (needLog) { System.out.println("logging method '" + method.getName() + "' after:" +mathBox.toString());}
        return obj;
    }
}
