package hometask1;

public class task1_2 {
    public static void main(String[] args) {
//***************************************************
// 2) Написать программу для поиска минимального из 4 чисел.
        int vV1 = 0, vV2 = -44, vV3 = -5, vV4 = 3;
        int vMin = Math.min(vV1, vV2);
        vMin = Math.min(vMin, vV3);
        vMin = Math.min(vMin, vV4);
        System.out.println("Минимальное число:" + vMin);
    }
}
