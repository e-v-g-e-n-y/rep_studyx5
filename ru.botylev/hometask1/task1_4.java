package hometask1;

import java.util.Scanner;

public class task1_4 {
    public static void main(String[] args) {
//        4) Написать программу, которая выводит факториал для N чисел.
        Scanner in = new Scanner(System.in);
        System.out.println("Введите значение для расчёта факториала:");
        int vValue = Integer.valueOf(in.nextLine());
        int vRes = 1;
        for (int i = 2; i <= vValue; i++) {
            vRes = vRes * i;
        }
        System.out.println(vValue + "!=" + vRes);
    }
}
