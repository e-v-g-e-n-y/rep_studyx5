package hometask1;

import java.util.Scanner;

public class task1_3 {
    private static void t1(int pValue)
    {
        for (int i = 1; i <= 10; i++)
        {
            System.out.println(pValue + "x" + i + "=" + (pValue * i));
        }
    }
    public static void main(String[] args) {
//        3) Написать программу для вывода на экран таблицы умножения до введенного пользователем порога.
        Scanner in = new Scanner(System.in);
        System.out.println("Введите нижнюю границу диапазона:");
        int vMin = Integer.valueOf(in.nextLine());
        System.out.println("Введите верхнюю границу диапазона:");
        int vMax = Integer.valueOf(in.nextLine());
        for (int i = vMin; i <= vMax; i++)
        {
            //System.out.println("i=" + i);
            System.out.println();
            t1(i);
        }
    }
}
