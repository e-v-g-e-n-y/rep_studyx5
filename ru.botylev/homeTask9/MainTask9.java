package homeTask9;

import homeTask9.common.DocType;
import homeTask9.common.Item;
import homeTask9.docs.CustomDocument;
import homeTask9.docs.DocAdoption;
import homeTask9.docs.DocShipment;
import homeTask9.docs.EDocProcException;
import homeTask9.factories.DocFactory;
import homeTask9.factories.DocStatusFactory;

public class MainTask9 {
    // тест типовых операций с документами
    public static void Test(CustomDocument pDoc) throws EDocProcException {
        if (pDoc == null) {return;}

        // типовые операции с документом
        pDoc.startDoc();
        pDoc.Print();
        pDoc.addItem(new Item(11, 2.5F));
        pDoc.addItem(new Item(22, 5));
        pDoc.addItem(new Item(33, 20));
        pDoc.Print();
        pDoc.setItemQty(11,2F);
        pDoc.setItemQty(22,5);
        pDoc.setItemQty(33,20);
        pDoc.closeDoc();
        pDoc.Print();
    }
    public static void main(String[] args) throws EDocProcException {
        // выбор клиента
        int idClient = 1;
        // документ приёмки
        CustomDocument docArrival = DocFactory.createDocument(DocType.dotAdoption, idClient);
        Test(docArrival);
        // документ отгрузки
        CustomDocument docShipment = DocFactory.createDocument(DocType.dotShipment, idClient);
        Test(docShipment);
    }
}
