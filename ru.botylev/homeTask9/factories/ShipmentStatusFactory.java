package homeTask9.factories;

import homeTask9.common.DocStatusType;
import homeTask9.common.DocType;
import homeTask9.statuses.DocStatus;
import homeTask9.statuses.shipmentStatuses.ShipmentClosed;
import homeTask9.statuses.shipmentStatuses.ShipmentDraft;
import homeTask9.statuses.shipmentStatuses.ShipmentInProgress;

public class ShipmentStatusFactory extends CustomDocStatusFactory{
    private final DocType type = DocType.dotShipment;

    @Override
    protected DocStatus createDraft() {
        return new ShipmentDraft();
    }

    @Override
    protected DocStatus createInProgress() {
        return new ShipmentInProgress();
    }

    @Override
    protected DocStatus createClosed() {
        return new ShipmentClosed();
    }

    @Override
    public DocType getType() {
        return type;
    }

}
