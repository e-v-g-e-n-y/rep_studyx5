package homeTask9.factories;

import homeTask9.common.DocStatusType;
import homeTask9.common.DocType;
import homeTask9.docs.EDocProcException;
import homeTask9.statuses.DocStatus;

import java.util.HashMap;
import java.util.Map;

public class DocStatusFactory {
    private Map<DocType, CustomDocStatusFactory> factories = new HashMap<>();

    private void RegistrateFactory(CustomDocStatusFactory factory) {
        factories.put(factory.getType(), factory);
    }
    public DocStatusFactory() {
        RegistrateFactory(new ShipmentStatusFactory());
        RegistrateFactory(new AdoptionStatusFactory());
    }

    public DocStatus createStatus(DocType docType, DocStatusType statusType) {
        CustomDocStatusFactory factory = factories.get(docType);
        if (factory == null) {
            throw new EDocProcException("Незарегистрированный тип документа '" + docType.toString() + "'!");
        }
        return  factory.createStatus(statusType);
    }
}
