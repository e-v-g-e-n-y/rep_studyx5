package homeTask9.factories;

import homeTask9.common.DocStatusType;
import homeTask9.common.DocType;
import homeTask9.docs.EDocProcException;
import homeTask9.statuses.DocStatus;

public abstract class CustomDocStatusFactory {
    protected abstract DocStatus createDraft();
    protected abstract DocStatus createInProgress();
    protected abstract DocStatus createClosed();
    public abstract DocType getType();
    public DocStatus createStatus(DocStatusType statusType) {
        DocStatus status;
        switch(statusType) {
            case dsDraft:
                status = createDraft();
                break;
            case dsInProgress:
                status = createInProgress();
                break;
            case dsClosed:
                status = createClosed();
                break;
            default:
                throw new EDocProcException("Недопустимый статус '" + "' для фабрики типа '" + getType().toString() + "'");
                //break;
        }
        return status;
    }
}
