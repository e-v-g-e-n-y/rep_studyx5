package homeTask9.factories;

import homeTask9.common.DocType;
import homeTask9.docs.CustomDocument;
import homeTask9.docs.DocAdoption;
import homeTask9.docs.DocShipment;
import homeTask9.docs.EDocProcException;

public class DocFactory {
    private DocFactory() {
    }

    public static CustomDocument createDocument(DocType docType, int idClient) {
        switch(docType) {
            case dotAdoption:
                return new DocAdoption(idClient);
                //break;
            case dotShipment:
                return new DocShipment(idClient);
                //break;
            default:
                throw new EDocProcException("Не зарегистрированнй тип документа");
                //break;
        }
    }
}
