package homeTask9.factories;

import homeTask9.common.DocType;
import homeTask9.statuses.DocStatus;
import homeTask9.statuses.adoptionStatuses.AdoptionClosed;
import homeTask9.statuses.adoptionStatuses.AdoptionDraft;
import homeTask9.statuses.adoptionStatuses.AdoptionInProgress;

public class AdoptionStatusFactory extends CustomDocStatusFactory {
    private final static DocType type = DocType.dotAdoption;

    @Override
    protected DocStatus createDraft() {
        return new AdoptionDraft();
    }

    @Override
    protected DocStatus createInProgress() {
        return new AdoptionInProgress();
    }

    @Override
    protected DocStatus createClosed() {
        return new AdoptionClosed();
    }

    @Override
    public DocType getType() {
        return type;
    }
}
