package homeTask9.common;

// товар
public class Item {
    // для простоты всё - PUBLIC
    public int idItem;      // идентификатор товара
    private String itemName; // наименование товара
    public float qtyFact;   // фактическое кол-во
    public float qtyOrd;    // кол-во по док-ту

    private String loadItemName() {
        // имитация загрузки наименования из каталога
        return "Товар#" + this.idItem;
    }

    public Item(int idItem, float qtyOrd) {
        this.idItem = idItem;
        this.itemName = loadItemName();
        this.qtyFact = 0;
        this.qtyOrd = qtyOrd;
    }

    public String getItemName() {
        return itemName;
    }

    @Override
    public String toString() {
        return "Товар{" +
                "id=" + idItem +
                ", наимен-е='" + itemName + '\'' +
                ", кол-во фактич.=" + qtyFact +
                ", кол-во по док-там=" + qtyOrd +
                '}';
    }
}
