package homeTask9.common;

public enum DocStatusType {
    dsDraft{
        @Override
        public String toString() {
            return "Черновик";
        }
    },
    dsInProgress {
        @Override
        public String toString() {
            return "В работе";
        }
    },
    dsClosed {
        @Override
        public String toString() {
            return "Закрыт";
        }
    };
}
