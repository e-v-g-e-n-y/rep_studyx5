package homeTask9.docs;

import homeTask9.common.DocType;

// документ приёмки на склад
public class DocAdoption extends CustomDocument {
    @Override
    public DocType getDocType() {
        return DocType.dotAdoption;
    }

    @Override
    protected void canCloseDoc() throws EDocProcException {
        super.canCloseDoc();

        // предположим, что нельзя прнимать фактическое кол-во превышающее заказанное
        checkItemQtyExceed();
    }
    @Override
    public String docTypeName() {
        return "Приёмка";
    }

    //
    public DocAdoption(int idClient) {
        super(idClient);
    }

}
