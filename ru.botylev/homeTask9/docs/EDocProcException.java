package homeTask9.docs;

// базовый класс ошибок работы с документами
public class EDocProcException extends RuntimeException{
    public EDocProcException(String errorMessage) {
            super(errorMessage);
        }
}
