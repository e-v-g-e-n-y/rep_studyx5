package homeTask9.docs;

import homeTask9.DocumentProcessor;
import homeTask9.MainTask9;
import homeTask9.common.DocStatusType;
import homeTask9.common.DocType;
import homeTask9.common.Item;
import homeTask9.factories.CustomDocStatusFactory;
import homeTask9.statuses.DocStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// базовый класс для документа
public abstract class CustomDocument {
    // идентификатор контрагента
    private int idClient;
    private DocStatus status;

    public DocStatus getStatus() {
        return status;
    }

    public void setStatus(DocStatus status) {
        this.status = status;
    }

    public abstract DocType getDocType();

    // проверка возможности добавления товара
    protected void canAddItem(Item pItem) throws EDocProcException {
        // параметр pItem - как задел на будующее
        this.status.canAddItem(this);
    }
    // проверка корректности оформления документа и готовности к закрытию
    protected void canCloseDoc() throws EDocProcException {
        this.status.canCloseDoc(this);
    }
    // проверки на переходы статусов
    protected void canChangeStatus(DocStatus pNewStatus) throws EDocProcException {
        this.status.canChangeStatus(this, pNewStatus);
    }
    //
    protected void setStatus(DocStatusType pNewDocStatusType) throws EDocProcException {
        DocStatus pNewStatus = DocumentProcessor.statusFactory.createStatus(this.getDocType(), pNewDocStatusType);
        canChangeStatus(pNewStatus);
        this.status = pNewStatus;
    }
    public List<Item> itemList;

    // конструктор
    public CustomDocument(int idClient) {
        this.idClient = idClient;
        this.status = DocumentProcessor.statusFactory.createStatus(this.getDocType(),DocStatusType.dsDraft);
        this.itemList = new ArrayList<Item>();
    }
    //
    public void addItem(Item pItem)  throws EDocProcException {
        canAddItem(pItem);
        this.itemList.add(pItem);
    }
    // начало работы с документом
    public void startDoc() throws EDocProcException {
        setStatus(DocStatusType.dsInProgress);
    }
    // завершение работы с документом (закрытие)
    public void closeDoc() throws EDocProcException {
        // проверка допустимости закрытия документа
        canCloseDoc();
        setStatus(DocStatusType.dsClosed);
    }
    //
    public /*static*/ String docTypeName() {
        return "Неизвестный тип документа";
    }
    public void Print() {
        System.out.println("Тип документа:<" + this.docTypeName() + ">");
        System.out.println("Статус документа:<" + this.status.toString() + ">");
        System.out.println("Клиент:<" + this.idClient + ">");
        System.out.println("Список товаров:" + Arrays.toString(this.itemList.toArray()));
    }
    //
    protected void checkItemQtyExceed() throws EDocProcException {
        boolean vExcced = false;
        String vErrStr = "Факт.кол-во превышает кол-во по док-там для следующих товаров:" + "\n";
        for (Item cItem: itemList
             ) {
            if (cItem.qtyFact > cItem.qtyOrd) {
                vExcced = true;
                vErrStr = vErrStr + "арт.#" + cItem.idItem + " факт.кол-во=" + cItem.qtyFact + "; док.кол-во=" + cItem.qtyOrd;
            }
        }
        if (vExcced) { throw new EDocProcException(vErrStr);}
    }
    //
    public void setItemQty(int pIdItem, float pQtyFact) throws EDocProcException {
        for (int i = 0; i < itemList.size(); i++) {
            Item vItem = itemList.get(i);
            if (vItem.idItem == pIdItem) {
                vItem.qtyFact = pQtyFact;
                itemList.set(i, vItem);
                return;
            }
        }
        throw new EDocProcException("Товар не найден. Невозможно установить факт.кол-во для товара id=" + pIdItem);
    }
}
