package homeTask9.docs;

import homeTask9.common.DocType;
import homeTask9.common.Item;

// документ отгрузки товара
public class DocShipment extends CustomDocument {

    @Override
    public DocType getDocType() {
        return DocType.dotShipment;
    }

    @Override
    protected void canCloseDoc() throws EDocProcException {
        super.canCloseDoc();

        boolean vHasEmptyItems = false;
        String vErrStr = "Необходимо отгрузить следующие товары:" + "\n";
        // предположим, что нельзя закрывать не отгрузив товар, присутсвующий в накладной
        for (Item vItem: itemList
             ) {
            if (vItem.qtyFact == 0) {
                vHasEmptyItems = true;
                vErrStr = vErrStr + vItem.idItem + vItem.getItemName() + "\n";
            }
        }
        if (vHasEmptyItems){
            throw new EDocProcException(vErrStr);
        }
    }

    @Override
    public String docTypeName() {
        return "Отгрузка";
    }

    public DocShipment(int idClient) {
        super(idClient);
    }
}
