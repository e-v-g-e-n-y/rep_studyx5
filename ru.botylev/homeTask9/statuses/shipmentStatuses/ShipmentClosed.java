package homeTask9.statuses.shipmentStatuses;

import homeTask9.common.DocType;
import homeTask9.statuses.custom.CustomClosed;

public class ShipmentClosed extends CustomClosed {
    @Override
    public DocType getDocType() {
        return DocType.dotShipment;
    }
}
