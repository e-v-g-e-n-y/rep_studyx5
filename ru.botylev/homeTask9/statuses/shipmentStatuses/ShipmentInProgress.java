package homeTask9.statuses.shipmentStatuses;

import homeTask9.common.DocType;
import homeTask9.statuses.custom.CustomInProgress;

public class ShipmentInProgress extends CustomInProgress {
    @Override
    public DocType getDocType() {
        return DocType.dotShipment;
    }
}
