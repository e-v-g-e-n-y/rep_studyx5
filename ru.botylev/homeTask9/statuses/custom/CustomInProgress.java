package homeTask9.statuses.custom;

import homeTask9.common.DocStatusType;
import homeTask9.statuses.DocStatus;

public abstract class CustomInProgress extends DocStatus {

    @Override
    public DocStatusType getStatusType() {
        return DocStatusType.dsInProgress;
    }
}
