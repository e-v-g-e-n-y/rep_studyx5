package homeTask9.statuses.custom;

import homeTask9.common.DocStatusType;
import homeTask9.common.DocType;
import homeTask9.statuses.DocStatus;

public abstract class CustomDraft extends DocStatus {

    @Override
    public DocStatusType getStatusType() {
        return DocStatusType.dsDraft;
    }
}
