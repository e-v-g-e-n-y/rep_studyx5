package homeTask9.statuses.adoptionStatuses;

import homeTask9.common.DocType;
import homeTask9.statuses.custom.CustomDraft;

public class AdoptionDraft extends CustomDraft {
    @Override
    public DocType getDocType() {
        return DocType.dotAdoption;
    }
}
