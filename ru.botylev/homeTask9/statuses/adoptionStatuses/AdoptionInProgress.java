package homeTask9.statuses.adoptionStatuses;

import homeTask9.common.DocType;
import homeTask9.statuses.custom.CustomInProgress;

public class AdoptionInProgress extends CustomInProgress {
    @Override
    public DocType getDocType() {
        return DocType.dotAdoption;
    }
}
