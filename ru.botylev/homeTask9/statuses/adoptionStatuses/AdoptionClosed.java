package homeTask9.statuses.adoptionStatuses;

import homeTask9.common.DocType;
import homeTask9.statuses.custom.CustomClosed;

public class AdoptionClosed extends CustomClosed {

    @Override
    public DocType getDocType() {
        return DocType.dotAdoption;
    }
}
