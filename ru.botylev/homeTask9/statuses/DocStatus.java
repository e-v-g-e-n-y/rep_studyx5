package homeTask9.statuses;

import homeTask9.common.DocStatusType;
import homeTask9.common.DocType;
import homeTask9.docs.CustomDocument;
import homeTask9.docs.EDocProcException;

import java.util.Objects;

public abstract class DocStatus {
//    DocType       docType;
//    DocStatusType statusType;
//
//    public DocStatus(DocType docType, DocStatusType statusType) {
//        this.docType = docType;
//        this.statusType = statusType;
//    }

    public abstract DocType getDocType();

    public abstract DocStatusType getStatusType();

    @Override
    public String toString() {
        return getStatusType().toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DocStatus)) return false;
        DocStatus docStatus = (DocStatus) o;
        return getDocType() == docStatus.getDocType() &&
                getStatusType() == docStatus.getStatusType();
    }

    public void canAddItem(CustomDocument document) throws EDocProcException {
        // можем работать только с документом в статусе "в работе"
        if (document.getStatus().getStatusType() != DocStatusType.dsInProgress) {
            throw new EDocProcException("Невозможно добавление товара в документ, не находящийся в статусе В РАБОТЕ");
        }
    }

    public void canCloseDoc(CustomDocument document) throws EDocProcException {
        // нельзя закрывать док-т без товаров
        if (document.itemList.isEmpty()) {
            throw new EDocProcException("Нельзя закрывать документ без товаров");
        }
    }

    // проверки на переходы статусов
    public void canChangeStatus(CustomDocument document, DocStatus pNewStatus) throws EDocProcException {
        // проверка соответсвия нового статуса текущему бизнес-процессу (приёмка/списание)
        if (this.getDocType() != pNewStatus.getDocType()) {
            throw new EDocProcException("Не соответсвует тип документа у нового и старого статуса!");
        }

        // документ уже в указанном статусе
        if (pNewStatus.equals(this)) {
            throw new EDocProcException("Документ уже находится в статусе <" + this.toString() + ">");
        }
    }
}
