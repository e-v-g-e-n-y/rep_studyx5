package hometaskPattern2;

import hometaskPattern2.Items.Item;
import hometaskPattern2.Items.ItemProcException;
import hometaskPattern2.StateMashine.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {
    public Date getDtCreated() {
        return dtCreated;
    }

    public BigDecimal getPrice() {
        return price;
    }

    private int id;

    public Status getStatus() {
        return status;
    }

    private Status status;
    private Date dtCreated;
    private BigDecimal price;
    public List<Item> items = new ArrayList<>();

    public Order(int id) {
        this.id = id;
        this.dtCreated = new Date();
        this.price = null;
        ChangeStatus(new StatusEditing(this));
    }

    public void ChangeStatus(Status status) {
        if (status == null) {
            throw new DocProcessingException("Нельзя присвоить документу null-статус");
        }
        if ((this.status != null) && (status.getClass().getName().equals(this.status.getClass().getName()))) {
            throw new DocProcessingException("Документ уже находится в статусе '" + this.status.getName() + "'");
        }
        this.status = status;
    }

    public void ValidateOrder() {
        ItemVisitor visitor = new ItemVisitor();
        for (Item item:items
             ) {
            item.Validate(visitor);
        }
        if (visitor.GetErrList().isEmpty() == false) {
            // ошибки полученные из списка товаров
            throw new DocProcessingException(visitor.GetErrList().toString());
        }
        if ((visitor.getDeliveryCnt() > 0)&&(visitor.getGoodCnt() == 0)) {
            throw new DocProcessingException("Нельзя оформлять доставку при отсутствии в заказе товаров");
        }
    }
    protected void MakeOrderCalculation() {
        ItemVisitor visitor = new ItemVisitor();
        for (Item item:items
        ) {
            item.CalculatePrice(visitor);
        }
        this.price = visitor.getTotalPrice();
    }
    protected void DoCalculateOrder() {
        ValidateOrder();
        MakeOrderCalculation();
        ChangeStatus(new StatusCalculated(this));
    }
    public void CalculateOrder() {
        if (status.CanCalculateOrder()) {
            DoCalculateOrder();
        }
        else {
            throw new ItemProcException(status.getIllegalActionErrorMsg("Запрещен пересчёт заказа в статусе '"));
        }
    }
    public int getId() {
        return id;
    }

    protected void DoPayOrder() {
        ChangeStatus(new StatusPayed(this));
    }

    public void PayOrder() {
        if (status.CanPayOrder()) {
            DoPayOrder();
        } else {
            throw new ItemProcException(status.getIllegalActionErrorMsg("Запрещена оплата заказа в статусе '"));
        }
    }
    protected void DoIssueOrder() {
        ChangeStatus(new StatusClosed(this));
    }
    public void IssueOrder() {
        if (status.CanIssueOrder()) {
            DoIssueOrder();
        } else {
            throw new ItemProcException(status.getIllegalActionErrorMsg("Запрещена выдача заказа в статусе '"));
        }
    }
    protected void DoGoEditOrder() {
        //
        ChangeStatus(new StatusEditing(this));
    }
    public void GoEditOrder() {
        if (status.CanGoEditOrder()) {
            DoGoEditOrder();
        }
        else {
            throw new ItemProcException(status.getIllegalActionErrorMsg("Запрещен перевод заказа в статус редактрования из статуса '"));
        }
    }
    protected void DoRemoveItem(int itemIndex) {
        items.remove(itemIndex);
    }
    public void RemoveItem(int itemIndex) {
        if (status.CanRemoveItem()) {
            DoRemoveItem(itemIndex);
        }
        else {
            throw new ItemProcException(status.getIllegalActionErrorMsg("Запрещено удаление товаров в заказ в статусе '"));
        }
    }
    protected void DoAddItem(Item item) {
        items.add(item);
    }
    public void AddItem(Item item) {
        if (status.CanAddItem()) {
            DoAddItem(item);
        }
        else {
            throw new ItemProcException(status.getIllegalActionErrorMsg("Запрещено добавление товаров в заказ в статусе '"));
        }
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", status=" + status +
                ", dtCreated=" + dtCreated +
                ", price=" + price +
                ", items=" + items +
                '}';
    }
}
