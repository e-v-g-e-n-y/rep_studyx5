package hometaskPattern2;

import hometaskPattern2.Items.*;

import java.math.BigDecimal;

public class ItemFactory {
    public static Item getItem(int id) {
        //
        if ((id > 0) && (id <= 1000)) {
            return new Good(id,"Товар#" + id, new BigDecimal(id*1000));
        }
        else {
            switch (id) {
                case 1000001:{return new InsuranceItem(1000001,"Быстрый возврат/обмен 14 дней",10);}
                case 1000002:{return new InsuranceItem(1000002,"Расширение гарантии на 1 год",5);}
                case 1001001:{return new DeliveryItem(1001001,"Доставка (зона достаки #1)",500);}
                default: {
                    throw new ItemProcException("В каталоге отсутвует товар с запрошеным id=" + id);
                }
            }
        }
    }
}
