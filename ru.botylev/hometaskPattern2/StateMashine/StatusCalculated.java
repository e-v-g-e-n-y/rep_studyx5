package hometaskPattern2.StateMashine;

import hometaskPattern2.Order;

public class StatusCalculated extends Status {
    public StatusCalculated(Order order) {
        super(order);
    }

    @Override
    public String getName() {
        return "Просчитан";
    }

    @Override
    public boolean CanGoEditOrder() {
        return true;
    }

    @Override
    public boolean CanPayOrder() {
        return true;
    }
}
