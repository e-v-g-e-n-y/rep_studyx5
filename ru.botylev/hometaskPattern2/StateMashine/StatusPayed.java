package hometaskPattern2.StateMashine;

import hometaskPattern2.Order;

public class StatusPayed extends Status {
    public StatusPayed(Order order) {
        super(order);
    }

    @Override
    public String getName() {
        return "Оплачен";
    }

    @Override
    public boolean CanIssueOrder() {
        return true;
    }
}
