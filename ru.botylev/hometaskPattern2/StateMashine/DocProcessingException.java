package hometaskPattern2.StateMashine;

public class DocProcessingException extends RuntimeException {
    public DocProcessingException(String message) {
        super(message);
    }
}
