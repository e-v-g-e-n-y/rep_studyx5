package hometaskPattern2.StateMashine;

import hometaskPattern2.Order;

public class StatusClosed extends Status {
    public StatusClosed(Order order) {
        super(order);
    }

    @Override
    public String getName() {
        return "Закрыт/Обработан/Выдан";
    }
}
