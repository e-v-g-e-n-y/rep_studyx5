package hometaskPattern2.StateMashine;

import hometaskPattern2.Order;

public class StatusEditing extends Status {
    public StatusEditing(Order order) {
        super(order);
    }

    @Override
    public boolean CanCalculateOrder() {
        return true;
    }

    @Override
    public String getName() {
        return "Редактирование";
    }

    @Override
    public boolean CanAddItem() {
        return true;
    }
}
