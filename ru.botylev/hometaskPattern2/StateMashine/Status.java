package hometaskPattern2.StateMashine;

import hometaskPattern2.Order;

public abstract class Status {
    private final String MSG_CHECK_WORKFLOW = "Сверьтесь со схемой бизнес-процесса.";
    protected Order order;
    public Status(Order order) {
        this.order = order;
    }
    public abstract String getName();

    public boolean CanAddItem() {
        return false;
    }
    public boolean CanRemoveItem() {
        return false;
    }
    public boolean CanCalculateOrder() { return false; }
    public boolean CanGoEditOrder()    { return false; }
    public boolean CanPayOrder()       { return false; }
    public boolean CanIssueOrder()     { return false; }

    public String getIllegalActionErrorMsg(String msgRefix) {
        return msgRefix + getName() + "'." + MSG_CHECK_WORKFLOW;
    }

    @Override
    public String toString() {
        return "Status='" + getName() + "'";
    }
}
