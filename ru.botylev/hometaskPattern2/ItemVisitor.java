package hometaskPattern2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ItemVisitor {
    private BigDecimal orderTotalPrice;
    private int deliveryCnt;
    private int goodCnt;
    private List<String> validationErrs = new ArrayList<>();
    public void AddPrice(BigDecimal currPrice) {
        orderTotalPrice = orderTotalPrice.add(currPrice);
    }
    public BigDecimal getTotalPrice() {
        return orderTotalPrice;
    }
    public void AddErr(String strError) {
        validationErrs.add(strError);
    }
    public List<String> GetErrList() {
        return validationErrs;
    }
    public void AddDelivery(int deliveryQty) {deliveryCnt = deliveryCnt + deliveryQty; }
    public void AddGood(int goodQty) { goodCnt = goodCnt + goodQty; }
    public int getDeliveryCnt() { return deliveryCnt; }
    public int getGoodCnt() { return goodCnt; }
    public ItemVisitor() {
        deliveryCnt = 0;
        goodCnt = 0;
        orderTotalPrice = new BigDecimal(0);
    }
}
