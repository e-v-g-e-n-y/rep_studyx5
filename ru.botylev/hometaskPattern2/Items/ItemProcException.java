package hometaskPattern2.Items;

public class ItemProcException extends RuntimeException {
    public ItemProcException(String message) {
        super(message);
    }
}
