package hometaskPattern2.Items;

public class InsuranceItem extends ServicePercentagePrice {
    public InsuranceItem(int id, String itemName, int percent) {
        super(id, itemName, percent);
    }
}
