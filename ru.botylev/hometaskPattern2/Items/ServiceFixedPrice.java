package hometaskPattern2.Items;

import java.math.BigDecimal;

public class ServiceFixedPrice extends Service {
    public ServiceFixedPrice(int id, String itemName, BigDecimal price) {
        super(id, itemName, price);
    }

    @Override
    public BigDecimal CalculateOwnPrice() {
        return this.price;
    }
}
