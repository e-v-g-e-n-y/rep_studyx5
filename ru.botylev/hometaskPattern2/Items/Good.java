package hometaskPattern2.Items;

import java.math.BigDecimal;

public class Good extends Item {

    public Good(int id, String itemName, BigDecimal price) {
        super(id, itemName, price);
    }

    @Override
    public BigDecimal CalculateOwnPrice() {
        return price;
    }

    @Override
    public int getGoodQty() {
        return 1;
    }
}
