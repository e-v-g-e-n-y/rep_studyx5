package hometaskPattern2.Items;

import hometaskPattern2.ItemVisitor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public abstract class Item {
    protected int id;
    protected List<Item> subItems = new ArrayList<>();
    protected String itemName;
// убрано во избежании разрастания учебного проекта - считаем что весь товар проходит поштучно    protected int Qty;
    protected BigDecimal price;
    protected Item parentItem;
    protected void DoValidate(ItemVisitor visitor) {
        visitor.AddDelivery(this.getDeliveryQty());
        visitor.AddGood(this.getGoodQty());
    }
    public Item(int id, String itemName, BigDecimal price) {
        this.id = id;
        this.itemName = itemName;
        this.price = price;
    }

    public List<Item> getSubItems() {
        return subItems;
    }

    public void AddItem(Item item) {
        //
        subItems.add(item);
        item.parentItem = this;
    }
    public abstract BigDecimal CalculateOwnPrice();

    public BigDecimal CalculatePrice(ItemVisitor visitor) {
        //
        BigDecimal resPrice = CalculateOwnPrice();
        resPrice.setScale(2, BigDecimal.ROUND_DOWN);
        visitor.AddPrice(resPrice);
        for (Item sItem: subItems
             ) {
            //
            BigDecimal currPrice = sItem.CalculatePrice(visitor);
            //visitor.AddPrice(currPrice);
            resPrice = resPrice.add(currPrice);
        }
        return resPrice;
    }

    public boolean HasParent() {
        return (parentItem != null);
    }
    public void Validate(ItemVisitor visitor) {
        //
        DoValidate(visitor);
        for (Item item: subItems
             ) {
            item.Validate(visitor);
        }
    }
    public int getGoodQty() {
        return 0;
    }
    public int getDeliveryQty() {
        return 0;
    }
    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", itemName='" + itemName + '\'' +
                ", price=" + price +
                ", subItems=" + subItems +
                '}';
    }
}
