package hometaskPattern2.Items;

import hometaskPattern2.ItemVisitor;
import java.math.BigDecimal;

public class ServicePercentagePrice extends Service {
    protected int percent;

    public ServicePercentagePrice(int id, String itemName, int percent) {
        super(id, itemName,  new BigDecimal(0));
        this.percent = percent;
    }

    @Override
    public BigDecimal CalculateOwnPrice() {
        return new BigDecimal((double)percent/100).multiply(parentItem.CalculateOwnPrice());
    }

    @Override
    public void DoValidate(ItemVisitor visitor) {
        super.DoValidate(visitor);
        if (!HasParent()) {
             visitor.AddErr("Услуга с процентной ценой не может быть добавлена без ссылки на товар!");
        }
    }
}
