package hometaskPattern2.Items;

import java.math.BigDecimal;

public abstract class Service extends Item {
    public Service(int id, String itemName, BigDecimal price) {
        super(id, itemName, price);
    }

    @Override
    public void AddItem(Item item) {
        throw new ItemProcException("Связка 'Услуга'-'Услуга' запрещена!");
    }
}
