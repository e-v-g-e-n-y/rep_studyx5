package hometaskPattern2.Items;

import hometaskPattern2.ItemVisitor;

import java.math.BigDecimal;

public abstract class ServiceNeedParent extends Service {
    public ServiceNeedParent(int id, String itemName, BigDecimal price) {
        super(id, itemName, price);
    }
    @Override
    public void Validate(ItemVisitor visitor) {
        //
        if (subItems.isEmpty() == false) {
            return;
        }
    }
}
