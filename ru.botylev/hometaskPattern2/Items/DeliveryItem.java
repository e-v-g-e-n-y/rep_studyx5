package hometaskPattern2.Items;

import java.math.BigDecimal;

public class DeliveryItem extends ServiceFixedPrice {
    public DeliveryItem(int id, String itemName, int price) {
        super(id, itemName, new BigDecimal(price));
    }

    @Override
    public int getDeliveryQty() {
        return 1;
    }
}
