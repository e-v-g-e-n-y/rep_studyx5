package hometaskPattern2;

import hometaskPattern2.Items.Item;

public class Main14 {
    public static void main(String[] args) {
        //
        Order ord = new Order(1);
        Item prod1 = ItemFactory.getItem(1);
        //переделать наследование страховок через класс "Нужен предок"
        prod1.AddItem(ItemFactory.getItem(1000001));
        prod1.AddItem(ItemFactory.getItem(1000002));
        ord.AddItem(prod1);
        ord.AddItem(ItemFactory.getItem(1001001));

        System.out.println(ord.toString());
        ord.CalculateOrder();
        System.out.println(ord.toString());
        System.out.println("order price=" + ord.getPrice().toString());
        // демонстрация запрета изменения состава заказа
        try {
            ord.AddItem(ItemFactory.getItem(2));
        } catch (Exception e) {
            e.printStackTrace();
        }
        ord.GoEditOrder();
        ord.AddItem(ItemFactory.getItem(2));
        ord.CalculateOrder();
        System.out.println(ord.toString());
        System.out.println("order price=" + ord.getPrice().toString());
        ord.PayOrder();
        System.out.println(ord.toString());

        ord.IssueOrder();
        System.out.println(ord.toString());
    }
}
