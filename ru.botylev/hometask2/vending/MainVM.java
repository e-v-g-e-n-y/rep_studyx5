package hometask2.vending;


import hometask2.vending.drinks.ColdDrink;
import hometask2.vending.drinks.Drink;
import hometask2.vending.drinks.Drink2;
import hometask2.vending.drinks.HotDrink;
import java.util.Scanner;

import static hometask2.vending.drinks.Drink2.Cola;
import static hometask2.vending.drinks.Drink2.Tea;

public class MainVM {

    public static void main(String[] args) {

        Drink2[] allDrinks = new Drink2[]{Tea, Cola};
        //Drink[] coldDrinks = new ColdDrink[]{new ColdDrink("Кола", 50)};

        VendingMachine vm = new VendingMachine(allDrinks);
        while (true) {
            try {
                // print a price
                vm.PrintPrice();
                System.out.println("Для выхода введите отрицательное значение");
                System.out.println("------------------------------------------");
                System.out.println("Текущий баланс:" + vm.getBalance());

                Scanner in = new Scanner(System.in);
                System.out.println("Внесите сумму:");
                double vMoney = Double.valueOf(in.nextLine());
                if (vMoney < 0) {
                    System.exit(0);
                }
                ;
                vm.addMoney(vMoney);
                System.out.println("Текущий баланс:" + vm.getBalance());
                System.out.println("Выберите напиток:");
                int vDrink = Integer.valueOf(in.nextLine());
                if (vDrink < 0) {
                    System.exit(0);
                }
                ;

                vm.giveMeADrink(vDrink);
            }
            catch (Exception e){
                System.out.println("Что-то пошло нет так:" + e.getMessage());
                System.out.println("Попробуйте ещё раз...");
            }
        }
    }
}