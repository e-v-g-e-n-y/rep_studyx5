package hometask2.vending.drinks;

public class HotDrink implements Drink {
    private String title;
    private double price;

    public HotDrink(String title, double price) {
        this.title = title;
        this.price = price;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "HotDrink{" +
                "title='" + title + '\'' +
                ", price=" + price +
                '}';
    }
}
