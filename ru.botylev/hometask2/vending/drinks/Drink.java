package hometask2.vending.drinks;

//public enum Drink {
//
//    HotDrink {
//        private String title;
//        private double price;
//
////    public HotDrink(String title, double price) {
////            this.title = title;
////            this.price = price;
////        }
//
//        @Override
//        public double getPrice() {
//            return price;
//        }
//
//        public void setPrice(double price) {
//            this.price = price;
//        }
//
//        @Override
//        public String getTitle() {
//            return title;
//        }
//    },
//
//    ColdDrink {
//        @Override
//        public double getPrice() {
//            return this.price;
//        }
//
//        public void setPrice(double price) {
//            this.price = price;
//        }
//
//        @Override
//        public String getTitle() {
//            return title;
//        }
//    };
//
//    private String title;
//    private double price;
//    public abstract double getPrice();
//    public abstract String getTitle();
//}
public interface Drink {
    double getPrice();
    String getTitle();
}