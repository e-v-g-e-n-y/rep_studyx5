package hometask2.vending.drinks;

public enum Drink2 {
    Tea {
        private static final String title = "Tea";
        private static final double price = 150;

        @Override
        public double getPrice() {
            return price;
        }

        @Override
        public String getTitle() {
            return title;
        }
        public String toString() {
            return "Drink{" +
                    "title='" + title + '\'' +
                    ", price=" + price +
                    '}';
        }
    },

    Cola {
        private static final String title = "Cola";
        private static final double price = 50;

        @Override
        public double getPrice() {
            return price;
        }

        @Override
        public String getTitle() {
            return title;
        }
        public String toString() {
            return "Drink{" +
                    "title='" + title + '\'' +
                    ", price=" + price +
                    '}';
        }
    };

    public abstract double getPrice();
    public abstract String getTitle();
}
