package hometask2.vending;


import hometask2.vending.drinks.Drink;
import hometask2.vending.drinks.Drink2;
import java.util.Arrays;

public class VendingMachine {
    private double money = 0;
    private Drink2[] drinks;

    public VendingMachine() {
    }

    public VendingMachine(Drink2[] drinks) {
        this.drinks = drinks;
    }

    public void addMoney(double money) {
        this.money = money;
    }

    public void PrintPrice() {
        //
        System.out.println(Arrays.toString(this.drinks));
    }

    private Drink2 getDrink(int key) {
        if (key < drinks.length){
            return drinks[key];
        } else {
            return null;
        }
    }

    public void giveMeADrink(int key) {
        if (this.money > 0) {
            Drink2 drink = getDrink(key);

            if (drink != null) {
                if (drink.getPrice() <= money) {
                    System.out.println("Возьмите ваш напиток: " + drink.getTitle());
                    money -= drink.getPrice();
                } else {
                    System.out.println("Недостаточно средств!");
                }
            }
        } else {
            System.out.println("Бесплатно не работаем!");
        }
    }

    public double getBalance() {return this.money;}

    public void setDrinks(Drink2[] drinks) {
        this.drinks = drinks;
    }
}
