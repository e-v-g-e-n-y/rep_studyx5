package hometask12.Common;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class CommonFunc {
    public static void FillArrayList(List<Integer> pArrayList, int pCnt) {
        pArrayList.clear();
        for (int i = 0; i < pCnt; i++) {
            // убираем последовательные числа и обеспечиваем одинаковый результат заполнения массива (вместо random)
            pArrayList.add(i % 15);
        }
    }
    public static BigInteger CalcFactorial(Integer pValue) {
        BigInteger resValue = BigInteger.valueOf(1);
        for (int i = 2; i <= pValue; i++) {
            resValue = resValue.multiply(BigInteger.valueOf(i));
        }
        return resValue;
    }
}
