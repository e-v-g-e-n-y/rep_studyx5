package hometask12.SingleThead;

import hometask12.Common.CommonFunc;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


public class Main {
    private static final int ARR_SIZE = 100_000;
    public static void main(String[] args) {
        List<Integer> SrcList = new ArrayList<>(ARR_SIZE);
        CommonFunc.FillArrayList(SrcList, ARR_SIZE);
        long msStart = System.currentTimeMillis();
        for (int i = 0; i < SrcList.size(); i++) {
            int srcValue = SrcList.get(i);
            BigInteger factorValue = CommonFunc.CalcFactorial(srcValue);
            System.err.println("[" + i + "]" + srcValue + "! = " + factorValue );
        }
        long msFinish = System.currentTimeMillis();
        System.err.println("msStart=" + msStart);
        System.err.println("msFinish=" + msFinish);
        System.err.println("in_work=" + (msFinish - msStart));
    }
}
