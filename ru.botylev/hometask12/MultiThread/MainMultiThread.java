package hometask12.MultiThread;

import hometask12.Common.CommonFunc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MainMultiThread {
    private static final int ARR_SIZE = 100_000;
    private static final int THREAD_CNT = 10;
    public static Lock lock;
    public static int index = 0;
    public static List<Integer> SrcList = new ArrayList<>(ARR_SIZE);

    public static void main(String[] args) throws InterruptedException {
        CommonFunc.FillArrayList(SrcList, ARR_SIZE);
        lock = new ReentrantLock();
        long msStart = System.currentTimeMillis();
        ExecutorService threadPool = Executors.newFixedThreadPool(THREAD_CNT);
        for (int i = 0; i < THREAD_CNT; i++) {
            threadPool.execute(new CalcThread());
        }

        threadPool.shutdown();
        threadPool.awaitTermination(4*60, TimeUnit.SECONDS);
        long msFinish = System.currentTimeMillis();
        System.err.println("msStart=" + msStart);
        System.err.println("msFinish=" + msFinish);
        System.err.println("in_work=" + (msFinish - msStart));
    }
}
