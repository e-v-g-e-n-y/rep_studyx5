package hometask12.MultiThread;

import hometask12.Common.CommonFunc;

import java.math.BigInteger;

public class CalcThread extends Thread {
    private int srcValue;

    @Override
    public void run() {
        int curIndex = 0;
        while (this.isInterrupted() == false) {
            try
            {
                MainMultiThread.lock.lockInterruptibly();
                try
                {
                    // работа с защищенным ресурсом
                    curIndex = MainMultiThread.index++;
                    if (MainMultiThread.index <= MainMultiThread.SrcList.size()) {
                        srcValue = MainMultiThread.SrcList.get(curIndex);
                    }
                    else {
                        break;
                    }
                }
                finally
                {
                    MainMultiThread.lock.unlock();
                }
            }
            catch (InterruptedException e)
            {
                System.err.println("Interrupted wait");
            }
            BigInteger factValue = CommonFunc.CalcFactorial(srcValue);
            System.err.println("Thread[" + this.getName() + "]:[" + curIndex + "]" + srcValue + "! = " + factValue);
        }
        System.err.println("Thread[" + this.getName() + "] finished!");
    }
}
