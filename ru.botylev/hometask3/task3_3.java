package hometask3;


class MyException extends Exception {
    public MyException(String errorMessage) {
        super(errorMessage);
    }
}
public class task3_3 {
    public static void main(String[] args) throws MyException {
        //
        System.out.println("Hello world");
        if (1 == 1) {
            throw new MyException("This is example of Exception");
        }
        System.out.println("Hello world2");
    }
}
