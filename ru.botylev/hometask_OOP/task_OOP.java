package hometask_OOP;

import hometask_OOP.common.Item;
import hometask_OOP.docs.CustomDocument;
import hometask_OOP.docs.Doc_Adoption;
import hometask_OOP.docs.Doc_Shipment;
import hometask_OOP.docs.EDocProcException;

///////////////////////////////////////////////////////////////////////////////////////////////
//             Пример использования ООП при реализации системы документа оборота склада.
///////////////////////////////////////////////////////////////////////////////////////////////
//Базовый класс документов CustomDocument содержит признаки:
//        - idClient  - идентификатор контрагента
//        - DocStatus - статус док-та (ЧЕРНОВИК, В РАБОТЕ, ЗАКРЫТ)
//        - itemList  - список товаров
// и методы:
//        protected void canAddItem(Item pItem) - проверка возможноти добавления товара
//        protected void canCloseDoc()          - проверка возможности завершения работы с док-том (закрытия)
//        protected void canChangeStatus(DocStatus pNewStatus) - проверка возможности перехода в указанный стаутс
//        protected void setStatus(DocStatus pNewDocStatus) - переход док-та в указанный статус
//        public void startDoc() - перевести документ в статус "В РАБОТЕ"
//        public void closeDoc() - перевести док-т в статус "ЗАКРЫТ"
//        public String docTypeName() - тип док-та
//        public void Print   - вывод инфы о док-те
//        protected void checkItemQtyExceed() - проверка превышения факт.кол-ва на кол-во по док-там.
//        public void setItemQty(int pIdItem, float pQtyFact) - установить факт. кол-во для товара
//, которые являются общими для всех документов.
//
// Особенности работы, свойственные каждому из типов документов, реализуют уже классы наследники (
//        Doc_Adoption - док-т приёма на склад
//        Doc_Shipment - док-т отгрузки со склада
//). Например, в методах:
//         protected void canAddItem(Item pItem) - проверка возможноти добавления товара
//         protected void canCloseDoc()          - проверка возможности завершения работы с док-том (закрытия)
//         protected void canChangeStatus(DocStatus pNewStatus) - проверка возможности перехода в указанный стаутс
///////////////////////////////////////////////////////////////////////////////////////////////

public class task_OOP {
    // тест типовых операций с документами
    public static void Test(CustomDocument pDoc) throws EDocProcException {
        if (pDoc == null) {return;}

        // типовые операции с документом
        pDoc.startDoc();
        pDoc.addItem(new Item(11, 2.5F));
        pDoc.addItem(new Item(22, 5));
        pDoc.addItem(new Item(33, 20));
        pDoc.setItemQty(11,2F);
        pDoc.setItemQty(22,15);
        pDoc.setItemQty(33,20);
        pDoc.closeDoc();
        pDoc.Print();
    }
    public static void main(String[] args) throws EDocProcException {
        // выбор клиента
        int idClient = 1;
        // документ приёмки
        CustomDocument docArrival = new Doc_Adoption(idClient);
        Test(docArrival);
        // документ отгрузки
        CustomDocument docShipment = new Doc_Shipment(idClient);
        Test(docShipment);
    }
}
