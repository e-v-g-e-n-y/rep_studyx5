package hometask_OOP.docs;

// базовый класс ошибок работы с документами
public class EDocProcException extends Exception {
    public EDocProcException(String errorMessage) {
            super(errorMessage);
        }
}
