package hometask_OOP.docs;

// документ приёмки на склад
public class Doc_Adoption extends CustomDocument {
    @Override
    protected void canCloseDoc() throws EDocProcException {
        super.canCloseDoc();

        // предположим, что нельзя прнимать фактическое кол-во превышающее заказанное
        checkItemQtyExceed();
    }
    @Override
    public String docTypeName() {
        return "Приёмка";
    }

    //
    public Doc_Adoption(int idClient) {
        super(idClient);
    }

}
