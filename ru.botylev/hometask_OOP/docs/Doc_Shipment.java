package hometask_OOP.docs;

import hometask_OOP.common.Item;

// документ отгрузки товара
public class Doc_Shipment extends CustomDocument {

    @Override
    protected void canCloseDoc() throws EDocProcException {
        super.canCloseDoc();

        boolean vHasEmptyItems = false;
        String vErrStr = "Необходимо отгрузить следующие товары:" + "\n";
        // предположим, что нельзя закрывать не отгрузив товар, присутсвующий в накладной
        for (Item vItem: itemList
             ) {
            if (vItem.qtyFact == 0) {
                vHasEmptyItems = true;
                vErrStr = vErrStr + vItem.idItem + vItem.getItemName() + "\n";
            }
        }
        if (vHasEmptyItems){
            throw new EDocProcException(vErrStr);
        }
    }

    @Override
    public String docTypeName() {
        return "Отгрузка";
    }

    public Doc_Shipment(int idClient) {
        super(idClient);
    }
}
