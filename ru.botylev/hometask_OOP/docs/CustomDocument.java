package hometask_OOP.docs;

import hometask_OOP.common.Item;
import hometask_OOP.common.DocStatus;

import java.util.ArrayList;
import java.util.Arrays;

// базовый класс для документа
public class CustomDocument {
    // идентификатор контрагента
    private int idClient;
    private DocStatus Status;
    // проверка возможности добавления товара
    protected void canAddItem(Item pItem) throws EDocProcException {
        // можем работать только с документом в статусе "в работе"
        if (this.Status != DocStatus.dsInProgress) {
            throw new EDocProcException("Невозможно добавление товара в документ, не находящийся в статусе В РАБОТЕ");
        }
    }
    // проверка корректности оформления документа и готовности к закрытию
    protected void canCloseDoc() throws EDocProcException {
        // нельзя закрывать док-т без товаров
        if (itemList.isEmpty()) {
            throw new EDocProcException("Нельзя закрывать документ без товаров");
        }
    };
    // проверки на переходы статусов
    protected void canChangeStatus(DocStatus pNewStatus) throws EDocProcException {
        if (pNewStatus == this.Status) {
            throw new EDocProcException("Документ уже находится в статусе <" + this.Status.toString() + ">");
        }
    }
    //
    protected void setStatus(DocStatus pNewDocStatus) throws EDocProcException {
        canChangeStatus(pNewDocStatus);
        this.Status = pNewDocStatus;
    }
    public ArrayList<Item> itemList;

    // конструктор
    public CustomDocument(int idClient) {
        this.idClient = idClient;
        this.Status = DocStatus.dsDraft;
        this.itemList = new ArrayList<Item>();
    }
    //
    public void addItem(Item pItem)  throws EDocProcException {
        canAddItem(pItem);
        this.itemList.add(pItem);
    }
    // начало работы с документом
    public void startDoc() throws EDocProcException {
        setStatus(DocStatus.dsInProgress);
    }
    // завершение работы с документом (закрытие)
    public void closeDoc() throws EDocProcException {
        // проверка допустимости закрытия документа
        canCloseDoc();
        setStatus(DocStatus.dsClosed);
    }
    //
    public /*static*/ String docTypeName() {
        return "Неизвестный тип документа";
    }
    public void Print() {
        System.out.println("Тип документа:<" + this.docTypeName() + ">");
        System.out.println("Статус документа:<" + this.Status.toString() + ">");
        System.out.println("Клиент:<" + this.idClient + ">");
        System.out.println("Список товаров:" + Arrays.toString(this.itemList.toArray()));
    }
    //
    protected void checkItemQtyExceed() throws EDocProcException {
        boolean vExcced = false;
        String vErrStr = "Факт.кол-во превышает кол-во по док-там для следующих товаров:" + "\n";
        for (Item cItem: itemList
             ) {
            if (cItem.qtyFact > cItem.qtyOrd) {
                vExcced = true;
                vErrStr = vErrStr + "арт.#" + cItem.idItem + " факт.кол-во=" + cItem.qtyFact + "; док.кол-во=" + cItem.qtyOrd;
            }
        }
        if (vExcced) { throw new EDocProcException(vErrStr);}
    }
    //
    public void setItemQty(int pIdItem, float pQtyFact) throws EDocProcException {
        for (int i = 0; i < itemList.size(); i++) {
            Item vItem = itemList.get(i);
            if (vItem.idItem == pIdItem) {
                vItem.qtyFact = pQtyFact;
                itemList.set(i, vItem);
                return;
            }
        }
        throw new EDocProcException("Товар не найден. Невозможно установить факт.кол-во для товара id=" + pIdItem);
    }
}
