package hometask_OOP.common;

public enum DocStatus{
    dsDraft{
        @Override
        public String toString() {
            return "Черновик";
        }
    },
    dsInProgress {
        @Override
        public String toString() {
            return "В работе";
        }
    },
    dsClosed {
        @Override
        public String toString() {
            return "Закрыт";
        }
    };
    public abstract String toString();
}
