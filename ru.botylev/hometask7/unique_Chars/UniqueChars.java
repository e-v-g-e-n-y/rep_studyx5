package hometask7.unique_Chars;

import java.util.HashMap;
import java.util.Scanner;

public class UniqueChars {
    private String srcText;
    private HashMap<String, Integer> charContent = new HashMap<>();
    public String getText() {
    //    return "TODO  - realize method calculate and change method getText()";
        return charContent.toString();
    }

    public void setText(String pText) {
        this.srcText = pText;
        charContent.clear();
    }

    public void calculate() {
        for (int i = 0; i < srcText.length(); i++) {
            String item = srcText.substring(i,i+1);
            Integer vCnt = charContent.getOrDefault(item, 0);
            if (vCnt > 0) {
                charContent.replace(item, ++vCnt);
            } else charContent.put(item, ++vCnt);
        }
    }
}
