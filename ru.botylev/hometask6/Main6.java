package hometask6;

import hometask6.Basket.EBasketException;
import hometask6.Basket.BasketJavaImpl;

public class Main6 {

    public static void main(String[] args) {
        // данная процедура создана исключительно для тестирования реализации корзины
        BasketJavaImpl myBasket = new BasketJavaImpl();
        try {
            System.out.println("myBasket.addProduct(\"t1\", 0)");
            myBasket.addProduct("t1", 0);
        }
        catch (EBasketException e){
            e.printStackTrace();
        }
        try {
            System.out.println("myBasket.getProductQuantity(\"t1\")");
            int vErrQty = myBasket.getProductQuantity("t1");
        }
        catch (EBasketException e){
            e.printStackTrace();
        }
        System.out.println("myBasket.addProduct(\"t1\", 1)");
        myBasket.addProduct("t1", 1);
        System.out.println("myBasket.addProduct(\"t2\", 5)");
        myBasket.addProduct("t2", 5);
        System.out.println("myBasket.addProduct(\"t1\", 2)");
        myBasket.addProduct("t1", 2);
        System.out.println("myBasket.addProduct(\"t4\", 4)");
        myBasket.addProduct("t4", 4);
        System.out.println(myBasket.toString());
        System.out.println("Список через Interface:" + myBasket.getProducts().toString());
        System.out.println("myBasket.updateProductQuantity(\"t4\", 10)");
        myBasket.updateProductQuantity("t4", 10);
        System.out.println("myBasket.updateProductQuantity(\"t2\", 0)");
        myBasket.updateProductQuantity("t2", 0); // должен удалиться
        System.out.println(myBasket.toString());
        System.out.println("myBasket.updateProductQuantity(\"t3\", 7)");
        myBasket.updateProductQuantity("t3", 7);
        System.out.println(myBasket.toString());
        System.out.println("myBasket.removeProduct(\"t3\")");
        myBasket.removeProduct("t3");
        System.out.println(myBasket.toString());
        int vQty = myBasket.getProductQuantity("t1");
        System.out.println("Кол-во по товару\"t1\"=" + vQty);
        System.out.println("myBasket.clear()");
        myBasket.clear();
        System.out.println(myBasket.toString());
    }
}
