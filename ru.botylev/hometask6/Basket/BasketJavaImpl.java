package hometask6.Basket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BasketJavaImpl implements Basket {
    private HashMap<String, Integer> products = new HashMap<>();

    @Override
    public void addProduct(String product, int quantity) {
        if (quantity == 0) {
            throw new EBasketException("Нельзя добавлять товар с нулевым количеством");
        }
        int vQty = products.getOrDefault(product, 0);
        if (vQty > 0) { products.replace(product, vQty + quantity); }
        else { products.put(product, quantity); }
    }

    @Override
    public void removeProduct(String product) {
        products.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        if (quantity == 0) {
            removeProduct(product);
            return;
        }
        if (products.containsKey(product)) { products.replace(product, quantity);}
          else {products.put(product, quantity);}
    }

    @Override
    public void clear() {
        products.clear();
    }

    @Override
    public List<String> getProducts() {
        ArrayList<String> res = new ArrayList();
        res.addAll(products.keySet());
        return res;
    }

    @Override
    public int getProductQuantity(String product) {
        if (products.containsKey(product)) {
            return products.getOrDefault(product, 0);
        } else {
            throw new EBasketException("Указанный продукт отсутствует в корзине");
        }
    }

    @Override
    public String toString() {
        return "Состав корзины:" + products.toString();
    }
}
