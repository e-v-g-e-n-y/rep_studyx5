package hometask13.pkg2ObjectLoader;

import java.io.IOException;

public class Main13_2 {
    public static void main(String[] args) throws IOException, IllegalAccessException, NoSuchFieldException, ClassNotFoundException, InstantiationException {
        MyObject myObj = new MyObject();
        myObj.myInt = 324243;
        myObj.myString = "This is a test string";

        ObjectWriter objWriter = new ObjectWriter();
        objWriter.SaveToFile(myObj, "d://JAVA_DEBUG//res2.txt");

        Object myObj2 = objWriter.LoadFromFile("d://JAVA_DEBUG//res2.txt");
        System.out.println(myObj2.toString());
    }
}