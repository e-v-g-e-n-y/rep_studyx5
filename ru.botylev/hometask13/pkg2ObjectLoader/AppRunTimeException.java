package hometask13.pkg2ObjectLoader;

public class AppRunTimeException extends RuntimeException {
    public AppRunTimeException(String message) {
        super(message);
    }
}
