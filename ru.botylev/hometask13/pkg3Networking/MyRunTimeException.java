package hometask13.pkg3Networking;

public class MyRunTimeException extends RuntimeException {
    public MyRunTimeException(String message) {
        super(message);
    }
}
