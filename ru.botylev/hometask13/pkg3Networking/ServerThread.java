package hometask13.pkg3Networking;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerThread extends Thread{
    private ServerSocket sSocket;
    public final int PORT;

    public ServerThread(int port) throws IOException {
        PORT = port;
        try {
            sSocket = new ServerSocket(PORT);
        }
        catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void run() {
        try {
            System.err.println(sSocket);
            try (Socket socket = sSocket.accept();
                 BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                 PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true)) {
                System.err.println(socket);
                while (true) {
                    String str = in.readLine();
                    if (str.equals("END"))
                        break;
                    System.out.println(">>" + str);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        finally {
            try {
                sSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
