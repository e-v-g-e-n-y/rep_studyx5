package hometask13.pkg3Networking;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class ClientServerApp {
    private static PrintWriter out;
    private static Socket socket = null;
    public static void StartClient(String pRemoteHost, int RemotePort) throws IOException {
        InetAddress addr = InetAddress.getByName(pRemoteHost);
        System.out.println(addr);
        socket = new Socket(addr, RemotePort);
        System.out.println(socket);
        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
    }
    public static void StopClient() throws IOException {
        socket.close();
    }
    public static void main(String[] args) throws InterruptedException, IOException {
        Scanner inConsole = new Scanner(System.in);
        ServerThread Server = null;
        boolean vServerStarted = true;
        do {
            try {
                // запрос порта для сервера
                System.out.println("Инициализация сервера");
                System.out.println("Введите порт для серверной части:");
                int vPort = inConsole.nextInt();
                // старт сервера
                Server = new ServerThread(vPort);
                Server.start();
                vServerStarted = true; // иначе не пройдёт дальше, свалившись на первых инициализациях
            } catch (Exception e) {
                e.printStackTrace();
                vServerStarted = false;
                System.out.println("Ошибка при создании сервера. Попробуйте повторить инициализацию.");
            }
        } while (vServerStarted == false);

        // запрос порта для УДАЛЁННОГО клиента
        boolean vClientStarted = false;
        do {
            try {
                String vRemoteHost = "127.0.0.1";
                System.out.println("Введите порт удалённого сервера:");
                int vRemotePort = inConsole.nextInt();

                // запуск клиента
                StartClient(vRemoteHost, vRemotePort);
                vClientStarted = true;
            }
            catch (Exception e) {
                //
                e.printStackTrace();
                vClientStarted = false;
            }
        }
        while (vClientStarted == false);
        System.out.println("Ready to chat! Enter your message:");

        // отправка сообщений
        boolean isWorking = true;
        while (isWorking) {
            //
            String strMsg = inConsole.nextLine();
            if (strMsg.isEmpty()){continue;}
            System.out.println("<<" + strMsg);
            out.println(strMsg);
            if (strMsg.equalsIgnoreCase("END")) {
                isWorking = false;
            }
        }

        // остановка клиента
        StopClient();

        // остановка сервера
        Server.interrupt();
        Server.join();
        System.out.println("Работа приложения завершена");
    }
}
