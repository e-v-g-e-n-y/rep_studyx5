package hometask13.pkg1TextParser;

import java.io.FileNotFoundException;

public class Main13_1 {
    // константы для путей и имён файлов
    public static final String WORKINGFOLDER = "d://JAVA_DEBUG//";
    public static final String SRC_FILENAME = "note.txt"; // имя исходного файла
    public static final String RES_FILENAME = "res.txt"; // имя файла с результатами

    public static void main(String[] args) throws FileNotFoundException {
        TxtParser parser = new TxtParser();
        parser.parseTxtFile(WORKINGFOLDER + SRC_FILENAME);

        // print result for debug
        parser.debugPrintList();

        // save to file
        parser.saveToFile(WORKINGFOLDER + RES_FILENAME);

        // end
        System.out.println("Application finished!");
    }
}
