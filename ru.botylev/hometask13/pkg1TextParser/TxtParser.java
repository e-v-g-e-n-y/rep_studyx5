package hometask13.pkg1TextParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class TxtParser {
    private SortedSet<String> stringList = new TreeSet<>();
    // разбор файла
    public void parseTxtFile(String pFileName) throws FileNotFoundException {
        //
        File file = new File(pFileName);
        try (Scanner scanner = new Scanner(file)) {
            scanner.useDelimiter("\\ |\n"); // в качестве разделителей используем "пробел" и "перевод каретки"
            while (scanner.hasNext()) {
                String  tmpStr = scanner.next();
                String  str = tmpStr.replace(".","").trim().toLowerCase();
                stringList.add(str);
                System.err.println("added:<" + str + ">");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw e;
        }
    }
    // вывод в консоль содержимого результирующего массива
    public void debugPrintList(){
        //
        System.out.println("result is:");
        for (String strItem: stringList
        ) {
            System.out.println(strItem);
        }
    }
    // сохранение в файл
    public void saveToFile(String pFileName){
        StringBuilder content = new StringBuilder();
        for (String strItem: stringList
             ) {
            content.append(strItem + System.getProperty("line.separator"));
        }
        try (FileOutputStream fileOutputStream = new FileOutputStream(pFileName)) {
            byte[] buffer = content.toString().getBytes();
                fileOutputStream.write(buffer, 0, buffer.length);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}
